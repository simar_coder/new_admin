import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd, ActivatedRoute, ActivatedRouteSnapshot } from '@angular/router';
// import { MatProgressBar, MatButton } from '@angular/material';
import { Validators, FormGroup, FormControl } from '@angular/forms';
import { ApiServiceService } from '../shared/api-service.service';
import { LocalStorageService } from '../shared/local-storage.service';
import { User } from '../shared/models/user';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  signinForm: any = FormGroup;
  registerForm: any = FormGroup;
  isLoading: boolean = false;
  userData: any = {};
  constructor(
    private router: Router, private apiservice: ApiServiceService,
  ) { }

  ngOnInit(): void {
    this.signinForm = new FormGroup({
      email: new FormControl('', Validators.required),
      password: new FormControl('', Validators.required),
      rememberMe: new FormControl(false)
    });
    this.registerForm = new FormGroup({
      firstName: new FormControl('', Validators.required),
      email: new FormControl('', Validators.required),
      password: new FormControl('', Validators.required),
    });

  }

  //login
  signIn() {
    this.isLoading = true
    this.apiservice.login(this.signinForm.value).subscribe(res => {
      this.userData = res;
      console.log('hi', this.userData)
      if (this.userData.statusCode === 200)
      // if (this.userData.statusCode === 200 && this.userData.data.role === "A")
       {
        localStorage.setItem('token', this.userData.data.token);
        localStorage.setItem('userData', JSON.stringify(this.userData.data));
        this.router.navigate(['dashboard/user-List']);
        this.isLoading = false;
      } 

      else {
        alert('Wrong username or password!');
        // return this.isLoading = false;
      };
    });
    // this.router.navigateByUrl('dashboard/user-List')
  }

  // signup
  register() {
      this.isLoading = true
      debugger;
      this.apiservice.signUp(this.registerForm.value).subscribe(res => {
        this.userData = res;
        console.log('register data', this.userData)
        if (this.userData.statusCode === 200)
         {
          this.router.navigate(['']);
      // this.router.navigateByUrl('')
          this.isLoading = false;
        } 
  
        else {
          alert('Wrong username or password!');
          // return this.isLoading = false;
        };
      });
    
  }

  // forgePassword
  forgetPassword() {
    this.router.navigateByUrl('/forget-password')
  }

}



import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';
import { User } from './models/user';
import { environment } from '../../environments/environment.prod';
@Injectable({
    providedIn: 'root'
})
export class ApiServiceService {
    root = environment.apiURL;
    userResponse: any;
    usersData: any;
    categoryResponse: any;
    tagResponse: any;
    ForumResponse: any;
    token: any = '';
    constructor(private http: HttpClient) { }

    // --------------------access token------------------------
    getHeaders() {
        this.token = localStorage.getItem('token');
        let header
        if (this.token != '') {
            header = {
                headers: new HttpHeaders({
                    'Content-Type': 'application/json',
                    'x-access-token': this.token
                })
            }
        } else {
            console.log('token not found')
        }
        return header;

    }
    getImageHeader() {
        let header
        if (this.token != '') {
            header = {
                headers: new HttpHeaders({
                    'enctype': 'multipart/form-data',
                    'Accept': 'application/json',
                    'x-access-token': this.token
                })
            }
        } else {
            console.log('token not found')
        }
        return header;
    }

    login(model: { email: string; password: string; }): Observable<User> {
        const subject = new Subject<User>();
        this.http.post(`${this.root}/users/login`, model).subscribe((responseData: any) => {
            subject.next(responseData);
        }, (error) => {
            subject.next(error.error);

        });

        return subject.asObservable();
    }

 

    signUp(model: { firstName: string; email: string; password: string; }): Observable<User> {
        const subject = new Subject<User>();
        this.http.post(`${this.root}/users/create`, model, this.getHeaders()).subscribe((responseData: any) => {
            subject.next(responseData);
        }, (error) => {
            subject.next(error.error);

        });
        return subject.asObservable();

        // return subject.asObservable();
    }

    usersList(): Observable<User> {
        const subject = new Subject<User>();
        this.http.get(`${this.root}/users/getUsers`).subscribe((responseData) => {
            this.categoryResponse = responseData;
            subject.next(this.categoryResponse.data);
        }, (error) => {
            subject.next(error.error);
        });
        return subject.asObservable();
    }

// delete User
    deleteUsers(id: any): Observable<User[]> {
        const subject = new Subject<User[]>();
        this.http.delete(`${this.root}/users/delete/${id}`).subscribe((responseData) => {
            this.userResponse = responseData;
            subject.next(this.userResponse);
        }, (error) => {
            subject.next(error.error);
        });
        return subject.asObservable();
    }


// delete Userq
changePass(id: any): Observable<User> {
    debugger;
    let a;
    const subject = new Subject<User>();
    this.http.post(`${this.root}/users/changePassword/${id}`, a,).subscribe((responseData) => {
        this.userResponse = responseData;
        subject.next(this.userResponse);
    }, (error) => {
        subject.next(error.error);
    });
    return subject.asObservable();
}
}



    // getUsers(data, no, size): Observable<Userr[]> {
    //     const subject = new Subject<Userr[]>();
    //     this.http.get(`${this.root}/users/list?pageNo=${no}&pageSize=${size}&role=${data}`, { headers: null }).subscribe((responseData) => {
    //         this.userResponse = responseData;
    //         subject.next(this.userResponse);
    //     }, (error) => {
    //         subject.next(error.error);
    //     });
    //     return subject.asObservable();
    // }

    // userActiveInActive(id, isActivated) {
    //     const subject = new Subject<Userr[]>();
    //     let m;
    //     this.http.put(`${this.root}/users/activeOrDeactive?id=${id}&isActivated=${isActivated}`, m, this.getHeaders()).subscribe(res => {
    //         this.userResponse = res;
    //         subject.next(this.userResponse);
    //     }, error => {
    //         subject.next(error.error);
    //     });
    //     return subject.asObservable();
    // }

    // getUserById(id): Observable<Userr[]> {
    //     const subject = new Subject<Userr[]>();
    //     this.http.get(`${this.root}/users/getById/${id}`,this.getHeaders()).subscribe((responseData) => {
    //         this.userResponse = responseData;
    //         subject.next(this.userResponse.data);
    //     }, (error) => {
    //         subject.next(error.error);
    //     });
    //     return subject.asObservable();
    // }

    // uploadUserImage(id, model): Observable<any> {
    //     const subject = new Subject<any>();
    //     this.http.put(`${this.root}/users/uploadProfilePic/${id}`, model, this.getImageHeader()).subscribe((response) => {
    //        subject.next(response);
    //     }, (error) => {
    //         subject.next(error.error);
    //     }
    //     );
    //     return subject.asObservable();
    // }

    // addUser(data): Observable<Userr[]> {
    //     const subject = new Subject<Userr[]>();
    //     this.http.post(`${this.root}/users/register`, data, { headers: null }).subscribe(res => {
    //         this.userResponse = res;
    //         subject.next(this.userResponse);
    //     }, error => {
    //         subject.next(error.error);
    //     });
    //     return subject.asObservable();
    // }

   



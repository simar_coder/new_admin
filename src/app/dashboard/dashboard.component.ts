import { ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { Router, NavigationEnd, ActivatedRoute, ActivatedRouteSnapshot } from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
 
  constructor(private router:Router) {
  }

  ngOnInit(): void {
  }
  profile(){
    debugger;
    this.router.navigateByUrl('dashboard/user-Profile')
  }
  logout(){
    localStorage.removeItem('token');
    localStorage.clear();
    this.router.navigateByUrl('')
  }
}

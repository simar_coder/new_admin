import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './dashboard.component';
import { UserListComponent } from './user-list/user-list.component';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { ChartComponent } from '../dashboard/chart/chart.component';

const routes: Routes = [
  { 
    path: '', component: DashboardComponent ,
  children: [
  {path: 'user-List', component: UserListComponent },
  {path: 'user-Profile', component: UserProfileComponent },
  {path: 'user-chart', component: ChartComponent },
  
]
},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
